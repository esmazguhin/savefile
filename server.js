const express = require('express');
const fs = require('fs');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const port = 3010;

app.use(bodyParser.json({limit: '100mb'}));

app.use(cors());

app.post('/upload', (req, res) => {
  let dir = req.body.dir;
  let data = req.body.data;

  if (!dir || !data) {
    return res.send({success: false});
  }

  let directory = __dirname + "/files/" + dir;
  if (!fs.existsSync(directory)){
    fs.mkdirSync(directory, {recursive: true});
  }

  let currentTime = process.hrtime();
  let filename = currentTime[0] + '' + currentTime[1];

  data = data.replace(/^data:image\/png;base64,/, "");
  fs.writeFile(directory + "/"+filename+".png", data, 'base64', function(err) {
    if(err) console.log(err);
  });

  return res.send({success: true});
})

app.listen(port, () => console.log(`App listening on port ${port}!`));